module "vpc-route-table" {
  source = "git::https://gitlab.com/cofls1107/route-table-module.git"

  account_id = var.account_id
  region     = var.region

  vpc_name    = var.vpc_filter["Name"]
  subnet_name = var.subnet_filter["Name"]

  vpc_id     = data.aws_vpc.this.id
  subnet_ids = data.aws_subnet_ids.this.ids

  current_id     = data.aws_caller_identity.current.account_id
  current_region = data.aws_region.current.name

  routing_rules = local.routing_rules
  
  tags = var.tags
}