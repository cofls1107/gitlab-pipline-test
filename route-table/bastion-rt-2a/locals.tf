# 현재 지원되는 라우팅 테이블 target type
# internet gateway    : igw-
# nat gateway         : nat-
# vpc endpoind        : vpce-
# trangit gateway     : tgw-
# managed prefix list : pl-
locals {
  routing_rules = {
    igw = {
      dst_cidr    = "0.0.0.0/0"
      target_id   = "${data.aws_internet_gateway.this.id}"
    },

  }
}