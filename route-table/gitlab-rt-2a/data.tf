data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_vpc" "this" {
  dynamic "filter" {
    for_each = var.vpc_filter # block를 생성할 정보를 담은 collection 전달, 전달 받은 수 만큼 block 생성
    iterator = tag # 각각의 item 에 접근할 수 있는 라벨링 부여, content block에서 tag 를 사용하여 접근   
    content { # block안에서 실제 전달되는 값들
      name   = "tag:${tag.key}"
      values = [tag.value] # "Name" 값이 string일 경우 [] 사용
    }
  }
}

data "aws_subnet_ids" "this" {
  vpc_id = data.aws_vpc.this.id 
  dynamic "filter" {
    for_each = var.subnet_filter
    iterator = tag
    content {
      name   = "tag:${tag.key}"
      values = [tag.value]
    }
  }
}

# data "aws_internet_gateway" "this" {
#   dynamic "filter" {
#     for_each = var.internet_gateway_filter
#     iterator = tag
#     content {
#       name   = "tag:${tag.key}"
#       values = [tag.value]
#     }
#   }
# }

#data "aws_nat_gateway" "this" {
#  dynamic "filter" {
#    for_each = var.nat_gateway_filter
#    iterator = tag
#    content {
#      name   = "tag:${tag.key}"
#      values = [tag.value]
#    }
#  }
#}