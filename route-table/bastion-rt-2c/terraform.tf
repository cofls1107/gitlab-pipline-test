terraform {
  required_version = ">= 1.1.2"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.71"
    }
  }

 # bucket과 key 생성 룰
  # kis-{mts, mps}-{dev, test, stage, prod}-tf-state-backend/${ACCOUNT_ID}/{dev, test, stage, prod}/${VPC_LINEUP}
  backend "s3" {
    bucket         = "kcl-terraform-state-backend"
    key            = "047054565215/dev/rt/bastion/2c/terraform.state"
    region         = "ap-northeast-2"
    dynamodb_table = "terraform-state-locks" # 다이나모 테이블 이름 변경 가능(필요 시)
    encrypt        = true
  }
}
#