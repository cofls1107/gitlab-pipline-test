variable "account_id" {
  description = "List of Allowed AWS account IDs"
  type        = string
}

variable "region" {
  description = "AWS Region"
  type        = string
}

variable "vpc_filter" {
  description = "Filters to select subnets"
  type        = map(string)
}

variable "subnet_filter" {
  description = "A list of subnet ID to associate with."
  type        = map(string)
}

variable "internet_gateway_filter" {
  description = "A list of internet gateway ID to associate with."
  type        = map(string)
}

variable "tags" {
  description = "tag map"
  type        = map(string)
}
