output "vpc-route-table" {
  description = "AWS VPC route table"
  value       = module.vpc-route-table
}