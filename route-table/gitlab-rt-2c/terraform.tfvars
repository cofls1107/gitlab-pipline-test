account_id = "047054565215" # 아이디 변경 필수
region     = "ap-northeast-2" # 리전 변경 필수

vpc_filter = {
  "Name" = "dev-test-vpc"
}

# 라우팅 테이블 이름은 서브넷 이름에 의해 자동 생성됨
# 예) dev-test-bastion-subnet-2a --> dev-test-bastion-route-table-2a 
# replace(each.value, "-subnet-", "-route-table-")
subnet_filter = {
  "Name" = "dev-test-gitlab-subnet-2c"
}

# internet_gateway_filter = {
#   "Name" = "dev-test-vpc-igw"
# }

# nat_gateway_filter = {
#  "Name" = "dev-test-vpc-natgw-2c"
# }

# 공통 tag, 생성되는 모든 리소스에 태깅
tags = {
  "CreatedByTerraform"     = "true"
  "TerraformModuleName"    = "terraform-aws-module-vpc-route-table"
  "TerraformModuleVersion" = "v1.0.0"
}