account_id = "047054565215" # 아이디 변경 필수
region     = "ap-northeast-2" # 리전 변경 필수
prefix     = "dev"
vpc_name   = "test" # 최종 VPC 이름: ${prefix}-${vpc_name}-vpc
vpc_cidr   = "11.0.0.0/16" # 원하는 대역으로 변경(/16 ~ /28 가능)
azs        = ["ap-northeast-2a", "ap-northeast-2c"] # AZ 개수와 서브넷(cidr)은 1:1 매핑, 개수는 달라도 되지만 빈 값은 올 수 없음 

enable_internet_gateway = true
enable_nat_gateway      = true # 사용할려면 enable_internet_gateway = true 필수

subnets = {
  bastion = {
    cidr         = ["11.0.2.0/24", "11.0.3.0/24"],
    ipv4_type    = "public"
    natgw_subnet = "yes" # 사용할려면 enable_nat_gateway = true 필수
  },
  gitlab = {
    cidr         = ["11.0.6.0/24", "11.0.7.0/24"],
    ipv4_type    = "private"
    natgw_subnet = "no"
  },
}

# 공통 tag, 생성되는 모든 리소스에 태깅
tags = {
  "CreatedByTerraform"     = "true"
  "TerraformModuleName"    = "terraform-aws-module-vpc-v2"
  "TerraformModuleVersion" = "v1.0.0"
}