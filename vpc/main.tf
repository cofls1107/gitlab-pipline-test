module "vpc" {
  source = "git::https://gitlab.com/cofls1107/vpc-module.git"

  account_id = var.account_id
  region     = var.region
  prefix     = var.prefix

  vpc_name = var.vpc_name
  vpc_cidr = var.vpc_cidr
  subnets  = var.subnets
  azs      = var.azs

  enable_internet_gateway = var.enable_internet_gateway
  enable_nat_gateway      = var.enable_nat_gateway
  
  current_id     = data.aws_caller_identity.current.account_id
  current_region = data.aws_region.current.name

  tags = var.tags
  #
  ##
  ####
}