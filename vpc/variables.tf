variable "region" {
  description = "AWS Region"
  type        = string
}

variable "account_id" {
  description = "Allowed AWS account IDs"
  type        = string
}

variable "prefix" {
  description = "prefix for aws resources and tags"
  type        = string
}

variable "vpc_name" {
  description = "VPC name"
  type        = string
}

variable "vpc_cidr" {
  description = "VPC default cidr"
  type        = string
}

variable "azs" {
  description = "Availability Zone List"
  type        = list
}

variable "tags" {
  description = "tag map"
  type        = map(string)
}

variable "subnets" {
  description = "subnet list"
  type        = map(any)
}

variable "enable_internet_gateway" {
  description = "internet gateway whether or not use"
  type        = bool
}

variable "enable_nat_gateway" {
  description = "nat gateway whether or not use"
  type        = bool
}
#